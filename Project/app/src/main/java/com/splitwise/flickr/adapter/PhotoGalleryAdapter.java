package com.splitwise.flickr.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;

import com.splitwise.flickr.R;
import com.splitwise.flickr.Utils.Utils;
import com.splitwise.flickr.activities.ViewPhotoActivity;
import com.splitwise.flickr.photogallery.model.Photos;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class PhotoGalleryAdapter extends RecyclerView.Adapter<PhotoGalleryAdapter.ViewHolder> implements Filterable{

    private List<Photos> itemList;
    private List<Photos> filterList;
    private Context context;

    OnLoadMoreListener loadMoreListener;
    boolean isLoading = false, isMoreDataAvailable = true;

    public PhotoGalleryAdapter(Context context, List<Photos> itemList) {
        this.itemList = itemList;
        this.filterList = itemList;
        this.context = context;
    }

    public void setItemList(List<Photos> itemList) {
        this.itemList = itemList;
        this.filterList = itemList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.photo_gallery_item, null);
        ViewHolder rcv = new ViewHolder(layoutView);
        return rcv;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {

        if(position>=getItemCount()-1 && isMoreDataAvailable && !isLoading && loadMoreListener!=null){
            isLoading = true;
            loadMoreListener.onLoadMore();
        }

        final Photos photos = filterList.get(position);
        if(photos != null && !TextUtils.isEmpty(Utils.getImageUrl(photos,Utils.ImageSize.SIZE_M))){
            Picasso.with(context).load(Utils.getImageUrl(photos,Utils.ImageSize.SIZE_M)).resize(200, 150)
                    .centerCrop().into(holder.imageView, new Callback() {
                @Override
                public void onSuccess() {
                    holder.imageView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Intent intent = new Intent(context, ViewPhotoActivity.class);
                            intent.putExtra(ViewPhotoActivity.INTENT_VIEW_PHOTO, photos);
                            context.startActivity(intent);
                        }
                    });
                }

                @Override
                public void onError() {

                }
            });

        }
    }

    @Override
    public int getItemCount() {
        return this.filterList.size();
    }

    @Override
    public Filter getFilter() {
        Filter filter = new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {

                FilterResults results = new FilterResults();
                ArrayList<Photos> FilteredArrayNames = new ArrayList<>();

                constraint = constraint.toString().toLowerCase();
                for (int i = 0; i < itemList.size(); i++) {
                    String title = itemList.get(i).getTitle();
                    if (title.toLowerCase().contains(constraint.toString()))  {
                        FilteredArrayNames.add(itemList.get(i));
                    }
                }
                results.count = FilteredArrayNames.size();
                results.values = FilteredArrayNames;

                return results;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
               filterList = (List<Photos>) filterResults.values;
                notifyDataChanged();
            }
        };

        return filter;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView imageView;
        public ViewHolder(View v) {
            super(v);
            imageView = (ImageView) v.findViewById(R.id.image);
        }
    }

    public void setMoreDataAvailable(boolean moreDataAvailable) {
        isMoreDataAvailable = moreDataAvailable;
    }

    public void notifyDataChanged(){
        notifyDataSetChanged();
        isLoading = false;
    }


    public interface OnLoadMoreListener{
        void onLoadMore();
    }

    public void setLoadMoreListener(OnLoadMoreListener loadMoreListener) {
        this.loadMoreListener = loadMoreListener;
    }
}