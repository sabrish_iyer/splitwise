package com.splitwise.flickr.photogallery.request;

/**
 * Created by sabri on 3/30/2017.
 */
public class RequestConstants {

    public static final String URL = "https://api.flickr.com/services/rest/";
    public static final String API_KEY = "6a8e0794c29c9c6641e639af0f2e0754";

    public interface Parameters {

        String METHOD = "method";
        String API_KEY = "api_key";
        String TAGS = "tags";
        String REQUEST_PER_PAGE = "per_page";
        String PAGE = "page";
        String IMAGE_SIZE = "image_size";
        String FORMAT = "format";
        String NO_JSON_CALLBACK = "nojsoncallback";
    }

    public interface ParamaterValues {

        String FORMAT_JSON = "json";

         interface METHOD{

            String METHOD_SEARCH = "flickr.photos.search";
            String METHOD_RECENT = "flickr.photos.getrecent";
        }

    }

}
