package com.splitwise.flickr.activities;

import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.splitwise.flickr.R;
import com.splitwise.flickr.Utils.Utils;
import com.splitwise.flickr.adapter.PhotoGalleryAdapter;
import com.splitwise.flickr.controller.SearchController;
import com.splitwise.flickr.photogallery.model.Photos;
import com.splitwise.flickr.photogallery.model.Stat;

import java.util.ArrayList;
import java.util.List;

public class GalleryActivity extends AppCompatActivity implements View.OnClickListener, SearchView.OnQueryTextListener {

    private RecyclerView mRecyclerView;
    private RecyclerView.LayoutManager mLayoutManager;
    private List<Photos> photosList;
    private int page = 0;
    private PhotoGalleryAdapter photoGalleryAdapter;
    private boolean firstTimeRequest = true;
    private ProgressBar loadMore;
    private String searchText;
    private SearchView searchView;
    private SearchController searchController;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gallery);
        initializeView();
        setAdapter();
        page = 1;
        searchController = new SearchController(this);
        getPopularPhotoRequest(searchText,page);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);

        final MenuItem searchItem = menu.findItem(R.id.action_search);
        searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
        searchView.setOnQueryTextListener(this);

        return true;
    }

    @Override
    public boolean onQueryTextChange(String query) {
        photoGalleryAdapter.getFilter().filter(query);
        searchText = query;
        return false;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        searchView.setQuery("", false);
        searchView.clearFocus();
        searchText = query;
        getPopularPhotoRequest(searchText, 1);
        return false;
    }

    private void setAdapter(){
        photosList = new ArrayList<>();
        photoGalleryAdapter = new PhotoGalleryAdapter(this, photosList);
        photoGalleryAdapter.setLoadMoreListener(new PhotoGalleryAdapter.OnLoadMoreListener() {
            @Override
            public void onLoadMore() {

                mRecyclerView.post(new Runnable() {
                    @Override
                    public void run() {
                        if(searchView != null && TextUtils.isEmpty(searchView.getQuery())) {
                            page += 1;
                            getPopularPhotoRequest(searchText,page);
                        }
                    }
                });
            }
        });

        mRecyclerView.setAdapter(photoGalleryAdapter);
    }

    private void initializeView(){
        findViewById(R.id.errorLayout).setOnClickListener(this);
        getSupportActionBar().setTitle(R.string.photo_gallery_title);
        mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        loadMore = (ProgressBar) findViewById(R.id.loadMore);
        mLayoutManager = new GridLayoutManager(this, 2);
        mRecyclerView.setLayoutManager(mLayoutManager);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.errorLayout:
                page = 1;
                getPopularPhotoRequest(searchText,page);
                break;
        }
    }


    private void populateAdapter(List<Photos> a_photosList){
        if(a_photosList.size()>0){
            this.photosList.addAll(a_photosList);
            photoGalleryAdapter.setItemList(this.photosList);
        }else{
            photoGalleryAdapter.setMoreDataAvailable(false);
            Toast.makeText(this,R.string.no_data_available,Toast.LENGTH_LONG).show();
        }
        photoGalleryAdapter.notifyDataChanged();
    }

    private void getPopularPhotoRequest(String searchTag, final int a_page){

        page = a_page;

        if(firstTimeRequest){
            requestInProgress();
        }else{
            loadMore.setVisibility(View.VISIBLE);
        }

        searchController.getPhotoRequest(searchTag, a_page, Utils.ImageSize.SIZE_M, new SearchController.ISearchController() {
            @Override
            public void onPhotoReceived(List<Photos> a_photosList) {
                if(a_page == 1) photosList.clear();
                populateAdapter(a_photosList);
                responseSuccess();
                firstTimeRequest = false;
            }

            @Override
            public void onErrorReceived(Stat error, String errorMessage) {
                if(firstTimeRequest)responseFailure();
            }
        });
    }

    private void responseSuccess(){
        findViewById(R.id.progress).setVisibility(View.GONE);
        findViewById(R.id.errorLayout).setVisibility(View.GONE);
        findViewById(R.id.listViewLayout).setVisibility(View.VISIBLE);
        loadMore.setVisibility(View.GONE);
    }

    private void responseFailure(){
        findViewById(R.id.progress).setVisibility(View.GONE);
        findViewById(R.id.errorLayout).setVisibility(View.VISIBLE);
        findViewById(R.id.listViewLayout).setVisibility(View.GONE);
    }

    private void requestInProgress(){
        findViewById(R.id.progress).setVisibility(View.VISIBLE);
        findViewById(R.id.errorLayout).setVisibility(View.GONE);
        findViewById(R.id.listViewLayout).setVisibility(View.GONE);
    }


}
