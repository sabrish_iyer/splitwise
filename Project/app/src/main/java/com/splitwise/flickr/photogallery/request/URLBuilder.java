package com.splitwise.flickr.photogallery.request;

import java.util.ArrayList;
import java.util.List;


/**
 * Simple utility class to conveniently build REST URLs. Facilitates construction of paths and parameters without
 * worrying about delimiters and empty values.
 */
public class URLBuilder {
	
	private final StringBuilder       mUrl;
	
	private final List<NameValuePair> mQueryParams    = new ArrayList<>();
	
	public URLBuilder(String host) {

		mUrl = new StringBuilder();
		mUrl.append(host);
	}
	
	public URLBuilder appendPathElement(String pathElement) {
		return appendPathElement(pathElement, true);
	}
	
	public URLBuilder appendPathElement(String pathElement, boolean addTrailingSlash) {
		if (pathElement == null || pathElement.toString().length() == 0) {
			return this;
		}
		
		if (mUrl.charAt(mUrl.length() - 1) != '/') {
			mUrl.append('/');
		}
		mUrl.append(pathElement);
		if (addTrailingSlash) {
			mUrl.append('/');
		}
		
		return this;
	}
	
	public URLBuilder appendPathElements(String[] pathElements) {
		return appendPathElements(pathElements, true);
	}
	
	public URLBuilder appendPathElements(String[] pathElements, boolean addTrailingSlash) {
		for (String pathElement : pathElements) {
			appendPathElement(pathElement, addTrailingSlash);
		}
		
		return this;
	}
	
	public URLBuilder addParam(NameValuePair nameValuePair) {
		mQueryParams.add(nameValuePair);
		
		return this;
	}
	
	@Override
	public String toString() {
		StringBuilder url = new StringBuilder(mUrl.toString());
		if (!mQueryParams.isEmpty()) {
			url.append("?");

			for(int i=0; i<mQueryParams.size(); i++){
				NameValuePair pair = mQueryParams.get(i);
				url.append(pair.getName()+"="+pair.getValue());
				if(i < mQueryParams.size()-1){
					url.append("&");
				}
			}
		}
		return url.toString();
	}
	
}