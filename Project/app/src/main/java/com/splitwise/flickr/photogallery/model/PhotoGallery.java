package com.splitwise.flickr.photogallery.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by sabri on 3/30/2017.
 */
public class PhotoGallery implements Parcelable {

    private int page;

    private int pages;

    private int perpage;

    private int total;

    private List<Photos> photo;


    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel out, int flags) {
        out.writeInt(page);
        out.writeInt(pages);
        out.writeInt(perpage);
        out.writeInt(total);
        out.writeTypedList(photo);
    }

    public static final Creator<PhotoGallery> CREATOR
            = new Creator<PhotoGallery>() {
        public PhotoGallery createFromParcel(Parcel in) {
            return new PhotoGallery(in);
        }

        public PhotoGallery[] newArray(int size) {
            return new PhotoGallery[size];
        }
    };

    private PhotoGallery(Parcel in) {
        page = in.readInt();
        pages = in.readInt();
        perpage = in.readInt();
        total = in.readInt();
        photo = in.createTypedArrayList(Photos.CREATOR);
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getPages() {
        return pages;
    }

    public void setPages(int pages) {
        this.pages = pages;
    }

    public int getPerpage() {
        return perpage;
    }

    public void setPerpage(int perpage) {
        this.perpage = perpage;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public List<Photos> getPhoto() {
        return photo;
    }

    public void setPhoto(List<Photos> photo) {
        this.photo = photo;
    }
}
