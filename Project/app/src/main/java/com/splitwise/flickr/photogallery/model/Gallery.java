package com.splitwise.flickr.photogallery.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by sabri on 3/30/2017.
 */
public class Gallery implements Parcelable {


    private PhotoGallery photos;
    private Stat stat;


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {

        parcel.writeParcelable(photos, i);
        parcel.writeString(this.stat.name());
    }

    public static final Creator<Gallery> CREATOR
            = new Creator<Gallery>() {
        public Gallery createFromParcel(Parcel in) {
            return new Gallery(in);
        }

        public Gallery[] newArray(int size) {
            return new Gallery[size];
        }
    };

    private Gallery(Parcel in) {
        photos = in.readParcelable(PhotoGallery.class.getClassLoader());
        stat = Stat.valueOf(in.readString());
    }

    public PhotoGallery getPhotos() {
        return photos;
    }

    public void setPhotos(PhotoGallery photos) {
        this.photos = photos;
    }

    public Stat getStat() {
        return stat;
    }

    public void setStat(Stat stat) {
        this.stat = stat;
    }
}
