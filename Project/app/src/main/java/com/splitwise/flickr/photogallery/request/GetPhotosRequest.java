package com.splitwise.flickr.photogallery.request;

import android.content.Context;
import android.text.TextUtils;

import com.splitwise.flickr.photogallery.model.Gallery;


/**
 * Created by sabri on 3/30/2017.
 */
public class GetPhotosRequest extends BaseRequest<Gallery> {

    private String method;
    private int page;
    private String[] tags;
    private String imageSize;
    private int requestPerPage = 100;

    public GetPhotosRequest(IRequestStatus<Gallery> responseListener, Context a_context) {
        super(responseListener, a_context);
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String[] getTags() {
        return tags;
    }

    public void setTags(String[] tags) {
        this.tags = tags;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public String getImageSize() {
        return imageSize;
    }

    public void setImageSize(String imageSize) {
        this.imageSize = imageSize;
    }

    public int getRequestPerPage() {
        return requestPerPage;
    }

    public void setRequestPerPage(int requestPerPage) {
        this.requestPerPage = requestPerPage;
    }

    @Override
    protected URLBuilder getUrlBuilder() {

        URLBuilder builder = super.getUrlBuilder();
        if(!TextUtils.isEmpty(method))
            builder.addParam(new NameValuePair(RequestConstants.Parameters.METHOD, method));

        if(!TextUtils.isEmpty(imageSize))
            builder.addParam(new NameValuePair(RequestConstants.Parameters.IMAGE_SIZE, imageSize));

        if(tags != null && tags.length > 0)
            builder.addParam(new NameValuePair(RequestConstants.Parameters.TAGS, TextUtils.join(",",tags)));

        if(page > 0){
            builder.addParam(new NameValuePair(RequestConstants.Parameters.PAGE, String.valueOf(page)));
        }

        if(requestPerPage > 0){
            builder.addParam(new NameValuePair(RequestConstants.Parameters.REQUEST_PER_PAGE, String.valueOf(requestPerPage)));
        }

        return  builder;
    }
}
