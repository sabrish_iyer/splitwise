package com.splitwise.flickr.photogallery.request;

/**
 * Created by sabri on 2/5/2017.
 */
public interface IRequestStatus<T> {
    public void onRequestCompleted(RequestResult<T> requestResult);
}
