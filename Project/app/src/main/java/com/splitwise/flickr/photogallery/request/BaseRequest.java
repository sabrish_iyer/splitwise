package com.splitwise.flickr.photogallery.request;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;

import java.lang.reflect.Array;
import java.lang.reflect.GenericArrayType;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Map;

/**
 * Created by sabri on 3/30/2017.
 */
public class BaseRequest<T> implements Response.ErrorListener, Response.Listener<T> {

    private IRequestStatus<T> mResponseListener;
    private final Class<T> responseClass;
    private RequestResult<T> mRequestResult;
    protected Context mContext;
    private RequestQueue queue;

    public BaseRequest(IRequestStatus<T> responseListener, Context a_context) {
        ParameterizedType genericSuperclass = (ParameterizedType) getClass().getGenericSuperclass();
        Type[] actualTypeArguments = genericSuperclass.getActualTypeArguments();
        try {
            responseClass = findActualClassArgument(actualTypeArguments[0]);
        } catch (RuntimeException e) {
            throw e;
        }

        queue = Volley.newRequestQueue(a_context);
        mRequestResult = new RequestResult<T>();
        mResponseListener = responseListener;
        mContext = a_context;
    }


    private  <T> Class<T> findActualClassArgument(Type actualTypeArgument) {
        // if the actual type itself is parameterized, get it's raw type, otherwise just cast
        if (actualTypeArgument instanceof ParameterizedType) {
            ParameterizedType parameterizedType = (ParameterizedType) actualTypeArgument;
            return (Class<T>) parameterizedType.getRawType();
        } else if (actualTypeArgument instanceof GenericArrayType) {
            GenericArrayType arrayType = (GenericArrayType) actualTypeArgument;
            return (Class<T>) Array.newInstance((Class<?>) arrayType.getGenericComponentType(), 0).getClass();
        } else {
            return (Class<T>) actualTypeArgument;
        }

    }

    protected String getUrl() {
        return getUrlBuilder().toString();
    }

    protected URLBuilder getUrlBuilder(){

        URLBuilder urlBuilder = new URLBuilder(RequestConstants.URL);

        urlBuilder.addParam(new NameValuePair(RequestConstants.Parameters.API_KEY,RequestConstants.API_KEY ));

        urlBuilder.addParam(new NameValuePair(RequestConstants.Parameters.FORMAT, RequestConstants.ParamaterValues.FORMAT_JSON));

        urlBuilder.addParam(new NameValuePair(RequestConstants.Parameters.NO_JSON_CALLBACK, String.valueOf(1)));

        return urlBuilder;

    }

    public void execute() {

        String url = getUrl();
        _executeWithURL(url);
    }

    protected void _executeWithURL(String url){
        GsonRequest<T> gsonRequest =  new GsonRequest(Request.Method.GET, url, responseClass, getHeaders(), this, this);
        queue.add(gsonRequest);

    }

    protected Map<String, String> getHeaders() {

        return null;
    }

    @Override
    public void onErrorResponse(VolleyError error) {

        mRequestResult.setRequestFailed(true);
        if (error != null) {
            mRequestResult.setErrorMessage(error.getMessage());
            mRequestResult.setError(error);
        }

        if (mResponseListener != null) {
            mResponseListener.onRequestCompleted(mRequestResult);
        }
    }

    @Override
    public void onResponse(T response) {

        if(response != null){
            sendSucessResponse(response);
        }else{
            sendFailResponse(response);
        }
    }

    private void sendFailResponse(T response) {
        VolleyError error = new VolleyError();
        onErrorResponse(error);
    }

    private void sendSucessResponse(T response) {

        mRequestResult.setRequestResponse(response);
        mRequestResult.setRequestFailed(false);

        if (mResponseListener != null) {
            mResponseListener.onRequestCompleted(mRequestResult);
        }
    }
}

