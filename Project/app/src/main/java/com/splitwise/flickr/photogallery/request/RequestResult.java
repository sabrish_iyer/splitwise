package com.splitwise.flickr.photogallery.request;

import com.android.volley.VolleyError;

import java.io.Serializable;


/**
 * Created by sabri on 2/5/2017.
 */
public class RequestResult<D> implements Serializable
{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -84356001714713132L;
	
	private boolean isRequestFailed = true;
	private int statusCode;
	private String errorMessage;
	private D requestResponse;
	private VolleyError error;
	
	public RequestResult()
	{
		
	}

	public boolean isRequestFailed()
	{
		return isRequestFailed;
	}
	
	public void setRequestFailed(boolean isRequestFailed)
	{
		this.isRequestFailed = isRequestFailed;
	}
	
	public String getErrorMessage()
	{
		return errorMessage;
	}
	
	public void setErrorMessage(String errorMessage)
	{
		this.errorMessage = errorMessage;
	}
	
	public D getRequestResponse()
	{
		return requestResponse;
	}
	
	public void setRequestResponse(D requestResponse)
	{
		this.requestResponse = requestResponse;
	}

	public VolleyError getError()
	{
		return error;
	}

	public void setError(VolleyError error)
	{
		this.error = error;
	}

	public int getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(int statusCode) {
		this.statusCode = statusCode;
	}
}
