package com.splitwise.flickr.controller;

import android.content.Context;
import android.text.TextUtils;

import com.splitwise.flickr.R;
import com.splitwise.flickr.photogallery.model.Gallery;
import com.splitwise.flickr.photogallery.model.Photos;
import com.splitwise.flickr.photogallery.model.Stat;
import com.splitwise.flickr.photogallery.request.GetPhotosRequest;
import com.splitwise.flickr.photogallery.request.IRequestStatus;
import com.splitwise.flickr.photogallery.request.RequestConstants;
import com.splitwise.flickr.photogallery.request.RequestResult;

import java.util.List;

/**
 * Created by sabri on 3/30/2017.
 */
public class SearchController {

    private Context context;

    public SearchController(Context a_context){

        context = a_context;
    }

    public void getPhotoRequest(String searchTag, final int a_page, String a_size, final ISearchController searchController){

        GetPhotosRequest request = new GetPhotosRequest(new IRequestStatus<Gallery>() {
            @Override
            public void onRequestCompleted(RequestResult<Gallery> requestResult) {

                if(requestResult != null && !requestResult.isRequestFailed() && requestResult.getRequestResponse() != null){

                    Gallery photoGallery = requestResult.getRequestResponse();
                    if(photoGallery.getStat() == Stat.ok) {
                        searchController.onPhotoReceived(photoGallery.getPhotos().getPhoto());
                    }else{
                        searchController.onErrorReceived(Stat.ERROR, context.getString(R.string.error_message_common));
                    }
                }else{
                   searchController.onErrorReceived(Stat.ERROR, context.getString(R.string.error_message_common));
                }

            }
        }, context);

        if(TextUtils.isEmpty(searchTag)){
            request.setMethod(RequestConstants.ParamaterValues.METHOD.METHOD_RECENT);
        }else{
            request.setMethod(RequestConstants.ParamaterValues.METHOD.METHOD_SEARCH);
            request.setTags(new String[]{searchTag});
        }
        request.setPage(a_page);
        request.setImageSize(a_size);
        request.execute();
    }

    public interface ISearchController{

        void onPhotoReceived(List<Photos> a_photosList);
        void onErrorReceived(Stat error, String errorMessage);
    }

}
