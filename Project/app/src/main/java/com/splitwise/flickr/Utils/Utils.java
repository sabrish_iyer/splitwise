package com.splitwise.flickr.Utils;

import com.splitwise.flickr.photogallery.model.Photos;

/**
 * Created by sabri on 3/30/2017.
 */
public class Utils {

    public interface ImageSize{

        public static String SIZE_M = "n";
        public static String SIZE_L = "h";
    }

    public static String getImageUrl(Photos photos, String size){

        if(photos != null) {
            return "https://farm" + photos.getFarm() + ".staticflickr.com/" + photos.getServer() +
                    "/" + photos.getId() + "_" + photos.getSecret() + "_" + size + ".jpg";
        }

        return null;
    }
}
